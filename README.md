# Web-Science
Web Science course taken in the MSc. Computer Science program of DIKU. 
This repo contains the assigned projects in the course. Both assignments are implemented in Python 2.x.

## Assignment 1 ##
The report for this project is the file named "Assignment-1/latex-sources/template-WS-project1.pdf". 

In this project we present an analysis in size estimation of the
indexable Web, using the approach presented by Lawrence
and Giles. The estimation was performed based a
query list consisting of 500 unique queries on the search en-
gines Bing, Baidu and Yandex, using an open source pro-
gram named "GoogleScraper". This project can scrape links
from various search engines and store the results in a database,
which we can later process using SQL.We present the method-
ology that was followed to perform the study, along with the
findings and the analysis of our results.

The methodology followed for this assignment was proposed in:

S. Lawrence and C. L. Giles. Searching the world wide
web. Science, 280(5360):98{100, 1998.

## Assignment 2 ##
The report for this project is the file named "Assignment-2/latex-sources/template-WS-project2.pdf". 

In this assignment we compare the implementation of recommender systems,
their different types and their applications. A recommender
system is an information filtering system that seeks to pre-
dict the rating or preference that a user would give to an
item.

The methodology followed to implement the three
system was proposed in: 

G. Adomavicius and A. Tuzhilin. Toward the next
generation of recommender systems: A survey of the
state-of-the-art and possible extensions. Knowledge and
Data Engineering, IEEE Transactions on,
17(6):734{749, 2005.
