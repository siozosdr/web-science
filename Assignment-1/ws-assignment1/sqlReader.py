__author__ = 'sokras'
import sqlite3
import codecs
import re
db = sqlite3.connect("./google_scraper.db")
cursor = db.cursor()
query1 = """select num_results_for_query
            from serp
            where search_engine_name == 'bing'
            and query != '-'
            and query != '-------------'
            and num_results_for_query != 0
            group by query"""
query2 =  """select num_results_for_query
            from serp
            where search_engine_name == 'google'
            and num_results_for_query != 0
            group by query"""

query3 =  """select num_results_for_query
            from serp
            where search_engine_name == 'baidu' and num_results_for_query != 0
            group by query"""
query4 =  """select num_results_for_query
            from serp
            where search_engine_name == 'yahoo' and num_results_for_query != 0
            group by query"""
lines = cursor.execute(query4)
data = cursor.fetchall()
sum_results = 0
i =1
# general regex: ((\d{1,3}(,\d{3})+)|(\b\d{1,3}\b)) results
for da in data:
    print i
    i+=1
    print da
    d = re.sub(r"\\u.{4}", "", str(da))
    res = re.search(r'(\d{1,3}(,\d{3})+)|(\b\d{1,3}\b)', str(d)).group()
    res = res.replace(',','')
    res = res.replace("results", '')
    print res
    sum_results+= int(res)
print 'Final:' + str(sum_results) + ' No. queries processed: ' + str(i-1)
db.close()
