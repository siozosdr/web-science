__author__ = 'sokras'
import numpy as np
import math
import operator

def read_file(filename):
    result = [line.rstrip('\n').split('::') for line in open(filename)]
    return result

def baseline1(data, test):
    ratings = data[:, 2]
    rating = sum(ratings)/len(ratings)
    result = []
    for i in range(len(test)):
        result.append(rating)
     # format result
    data = np.array(data)
    result = np.reshape(result, (len(test), 1))
    test = test[:, (0, 1)]
    result = np.column_stack((test, result))
    return result

def baseline2(data, test):
    userRatings = {}
    result = []
    for d in data:
        try:
            userRatings[int(d[0])].append(d[2])
        except KeyError:
            userRatings[int(d[0])] = []
            userRatings[int(d[0])].append(d[2])

    for t in test:
        temp = userRatings[int(t[0])]
        result.append(sum(temp) / len(temp))
     # format result
    data = np.array(data)
    result = np.reshape(result, (len(test), 1))
    test = test[:, (0, 1)]
    result = np.column_stack((test, result))
    return result

def baseline3(data, test):
    movieRatings = {}
    result = []
    for d in data:
        try:
            movieRatings[int(d[1])].append(d[2])
        except KeyError:
            movieRatings[int(d[1])] = []
            movieRatings[int(d[1])].append(d[2])
    for t in test:
        try:
            temp = movieRatings[int(t[1])]
        except KeyError:
            dummy = 2.5
            result.append(dummy)
            continue
        result.append(sum(temp) / len(temp))
     # format result
    data = np.array(data)
    result = np.reshape(result, (len(test), 1))
    test = test[:, (0, 1)]
    result = np.column_stack((test, result))
    return result


def hybridRecSys(userRatings, userProfiles, movieProfiles, test):
    userAverages = {}
    hybridResult = []
    for u in userRatings:
        tempList = [x[1] for x in userRatings[u]]
        userAverages[u] = sum(tempList) / len(tempList)
    # calculate results from both systems
    contentResult = contentRecSys(movieProfiles, userProfiles, test)
    collabResult = collaborativeRecSys(userRatings, userProfiles, test)

    # pick the best prediction from the two systems
    for i in range(len(test)):
        # check if the predictions are consistent with the previous ratings
        # of each user
        temp1 = np.abs(contentResult[i][2] - userAverages[contentResult[i][0]])
        temp2 = np.abs(collabResult[i][2] - userAverages[collabResult[i][0]])
        if temp1 < temp2:
            hybridResult.append(contentResult[i][2])
        else:
            hybridResult.append(collabResult[i][2])
    # format result
    test = np.array(test)
    hybridResult = np.reshape(hybridResult, (len(test), 1))
    hybridResult = np.column_stack((test[:, (0, 1)], hybridResult))
    return hybridResult


def contentRecSys(movieProfiles, userProfiles, test):
    result = []
    for t in test:
        mpindex = int(t[1])
        upindex = int(t[0])
        prediction = cosineSim(movieProfiles[mpindex], userProfiles[upindex])
        result.append(round(prediction*5, 1))

    # format result
    test = np.array(test)
    result = np.reshape(result, (len(test), 1))
    result = np.column_stack((test[:, (0, 1)], result))
    return result

def collaborativeRecSys(userRatings, userProfiles, test):
    average = 0
    k = 0
    result = []
    for t in test:
        # create list of user profiles that will be used for KNN
        # based on the users that rated the same movies as the test
        # user, this way i have all the neighbors for a single user
        # based on the common movies they rated.
        neighborsVectors = []
        neighborsIDs = []
        for u in userRatings:
            tempMovieIDs = [x[0] for x in userRatings[u]]
            if t[1] in tempMovieIDs:
                neighborsVectors.append(userProfiles[u])
                neighborsIDs.append(u)
        # i get the top 20 nearest users
        knnDict = getNeighbors(neighborsVectors, neighborsIDs, userProfiles[t[0]], 20)
        # calculate weighted average
        for v in knnDict:
            rating = [item for item in userRatings[v] if item[0] == t[1]]
            average += cosineSim(knnDict[v], userProfiles[t[0]]) * rating[0][1]
            k += np.abs(cosineSim(knnDict[v], userProfiles[t[0]]))
        weighted_average = average/k
        result.append(round(weighted_average, 1))

    # format result
    test = np.array(test)
    result = np.reshape(result, (len(test), 1))
    result = np.column_stack((test[:, (0, 1)], result))
    return result

'''
Helpers
'''
def nn_cross_validation(movieData, trainingData, nr_of_folds):
    rmseCon = {}  # map of rmse's
    rmseCol = {}
    rmseHyb = {}
    # split the data into 'nr_folds' chunks
    folds = [t.tolist() for t in np.array_split(np.array(trainingData), nr_of_folds)]

    for i in range(nr_of_folds):
        # take everything except i-th chunk
        train = folds[:i] + folds[i+1:]
        # merge chunks to one list
        train = [item for chunk in train for item in chunk]
        # take i-th chunk as test data
        test = folds[i]

        # calculate predictions
        mRat, uProf, mProf, uRat = tfidf(movieData, trainingData)
        contentResult = contentRecSys(mProf, uProf, test)
        # collabResult = collaborativeRecSys(uRat, uProf, test)
        # hybridResult = hybridRecSys(uRat, uProf, mProf, test)
        test = np.array(test)
        target = test[:, 2]
        # calculate RMSE for each system
        y = contentResult[:, 2]
        rmseTemp = RMSE(y, target)
        rmseCon[i] = rmseTemp
        #
        # y = collabResult[:, 2]
        # rmseTemp = RMSE(y, target)
        # rmseCol[i] = rmseTemp
        # #
        # y = hybridResult[:, 2]
        # rmseTemp = RMSE(y, target)
        # rmseHyb[i] = rmseTemp
    contentAverage = 0
    for key, value in rmseCon.iteritems():
        print 'RMSE in content for fold = {0:} : {1:.3f}'.format(key, value)
        contentAverage += value
    print('average:' + str(contentAverage/nr_of_folds))
    # for key, value in rmseCol.iteritems():
    #     print 'RMSE in collaborative for fold = {0:} : {1:.3f}'.format(key, value)
    # for key, value in rmseHyb.iteritems():
    #     print 'RMSE in hybrid for fold = {0:} : {1:.3f}'.format(key+1, value)

def nn_cross_validation_baselines(movieData, trainingData, nr_of_folds):
    rmseCon = {}  # map of rmse's
    rmseb2 = {}
    rmseHyb = {}
    # split the data into 'nr_folds' chunks
    trainingData = np.array(trainingData)
    # np.random.shuffle(trainingData)
    counter = 0
    folds = [t.tolist() for t in np.array_split(np.array(trainingData), nr_of_folds)]
    for i in range(nr_of_folds):
        counter += 20
        # take everything except i-th chunk
        train = folds[:i] + folds[i+1:]
        # merge chunks to one list
        train = [item for chunk in train for item in chunk]
        train = np.array(train)
        # take i-th chunk as test data
        test = folds[i]
        test = np.array(test)

        # calculate predictions
        # b1Result = baseline1(train, test)
        # b2Result = baseline2(train, test)
        b3Result = baseline3(train, test)
        counter += 20
        print('Counter: '+str(counter) + '%')
        target = test[:, 2]
        # calculate RMSE for each system
        # y = b1Result[:, 2]
        # rmseTemp = RMSE(y, target)
        # rmseCon[i] = rmseTemp
        #
        # y = b2Result[:, 2]
        # rmseTemp = RMSE(y, target)
        # rmseb2[i] = rmseTemp
        # #
        y = b3Result[:, 2]
        rmseTemp = RMSE(y, target)
        rmseHyb[i] = rmseTemp
    # b1Average = 0
    # for key, value in rmseCon.iteritems():
    #     print 'RMSE in b1 for fold = {0:} : {1:.3f}'.format(key, value)
    #     b1Average += value
    # print('average: '+str(b1Average/nr_of_folds))
    # b2Average = 0
    # for key, value in rmseb2.iteritems():
    #     print 'RMSE in b2 for fold = {0:} : {1:.3f}'.format(key, value)
    #     b2Average += value
    # print('average: '+str(b2Average/nr_of_folds))
    b3Average = 0
    for key, value in rmseHyb.iteritems():
        print 'RMSE in b3 for fold = {0:} : {1:.3f}'.format(key+1, value)
        b3Average += value
    print('average: '+str(b3Average/nr_of_folds))

def RMSE(y, target):
    rmse = 0
    for i in range(0, len(target)):
         rmse += (target[i] - y[i])**2
    rmse /= len(target)
    rmse = math.sqrt(rmse)
    return rmse

def getNeighbors(trainingSetVectors, trainingSetIDs, testInstance, k):
    distances = []
    for x in range(len(trainingSetVectors)):
        dist = cosineSim(testInstance, trainingSetVectors[x])
        distances.append((trainingSetVectors[x], dist, trainingSetIDs[x]))
    distances.sort(key=operator.itemgetter(1))

    neighbors = {}
    for x in range(k):
        # append (id, vector)
        try:
            key = distances[x][2]
        except IndexError:
            return neighbors
        neighbors[key] = distances[x][0]
    return neighbors

def cosineSim(v1, v2):
    # compute cosine similarity of v1 to v2: (v1 dot v1)/{||v1||*||v2||)
    sumxx, sumxy, sumyy = 0, 0, 0
    for i in range(len(v1)):
        x = v1[i]
        y = v2[i]
        sumxx += x*x
        sumyy += y*y
        sumxy += x*y
    return sumxy/math.sqrt(sumxx*sumyy)

def tfidf(movieData, training):
    genres = []
    nomovies = len(movieData)
    movieIDs = []
    for d in movieData:
        movieIDs.append(d[0])
    for d in movieData:
        tempList = d[2].split('|')
        genres.extend(tempList)
    genres = set(genres)
    genres = list(genres)
    movieVectors = {int(movieIDs[k]): [0 for _ in range(len(genres))] for k in range(len(movieIDs))}
    genresni = dict.fromkeys(genres, 0)
    print('movieData')
    for d in movieData:
        tempList = d[2].split('|')
        for t in tempList:
            genresni[t] += 1
    # for TFIDF
    for d in movieData:
        tempList = d[2].split('|')
        # TF = 0 if genre is not in movie description else 1
        for t in tempList:
            gindex = genres.index(t)
            mindex = int(d[0])
            movieVectors[mindex][gindex] = np.log(nomovies/genresni[t])
    # movieVector has the movie profiles after the loop above
    userRatings = {}
    movieProfiles = {}
    print('training')
    for t in training:

        #populate dict with movie and users that rated it
        if int(t[1]) in movieProfiles:
            movieProfiles[int(t[1])].append((int(t[0]), float(t[2])))
        else:
            mindex = int(t[1])
            movieProfiles[mindex] = [(int(t[0]), float(t[2]))]
        # populate dict with user and movies they rated
        if int(t[0]) in userRatings:
            # list of (movieID, rating)
            userRatings[int(t[0])].append((int(t[1]), float(t[2])))
        else:
            uindex = int(t[0])
            userRatings[uindex] = [(int(t[1]), float(t[2]))]
    userProfiles = {}
    # get profile for each user
    print('userratings')
    for userID in userRatings:
        tempVectorList = []
        for i in range(len(userRatings[userID])):
            tempMovieID = userRatings[userID][i][0]
            tempMovieVector = movieVectors[tempMovieID]
            # apply weights on the vector to calculate weighted mean average
            tempMovieVector = [x * 0.2 for x in tempMovieVector]
            tempVectorList.append(tempMovieVector)
        tempVectorList = np.array(tempVectorList)
        userProfiles[userID] = tempVectorList.mean(axis=0)
    return movieProfiles, userProfiles, movieVectors, userRatings

smallTraining = read_file('./data/trainingRatings.dat')
smallTraining = np.array(smallTraining)
smallTraining = smallTraining.astype(np.float)
movies = read_file('./data/movies.dat')
test = read_file(('./data/testFile.csv'))
test = np.array(test)
test = test.astype(np.int)
test = test.tolist()
#print baseline1(smallTraining)
#print baseline2(smallTraining)
#print baseline3(smallTraining)
print('read files')
# nn_cross_validation_baselines(movies, smallTraining, 5)
# print('calculating tfidf')
mRat, uProf, mProf, uRat = tfidf(movies, smallTraining)
# print('b1')
# b1 = baseline1(smallTraining, test)
# f = open('baseline1.csv', 'w')
# for t in b1:
#     f.write(str(int(t[0])) + '::' + str(int(t[1]))+','+str(t[2]) + '\n')
# f.close()
# print('b2')
# b2 = baseline2(smallTraining, test)
# f = open('baseline2.csv', 'w')
# for t in b2:
#     f.write(str(int(t[0])) + '::' + str(int(t[1]))+','+str(t[2]) + '\n')
# f.close()
# print('b3')
# b3 = baseline3(smallTraining, test)
# f = open('baseline3.csv', 'w')
# for t in b3:
#     f.write(str(int(t[0])) + '::' + str(int(t[1]))+','+str(t[2]) + '\n')
# f.close()

# mRat, uProf, mProf, uRat = tfidf(movies, smallTraining)
# test system 1
# f = open('result.csv', 'w')
# testResult = contentRecSys(mProf, uProf, test)
# for t in testResult:
#    f.write(str(t[0]) + '::' + str(t[1])+','+str(t[2]) + '\n')
# f.close()
# print('finished first')
# test system 2
f = open('result2.dat', 'w')
print('starting calculations')
testResult2 = collaborativeRecSys(uRat, uProf, test)
print 'finished'
for t in testResult2:
    f.write(str(int(t[0])) + '::' + str(int(t[1])) + ',' + str(t[2]) + '\n')
f.close()
# f = open('result2.dat', 'w')
# print('starting calculations')
# testResult2 = collaborativeRecSys(mRat, uRat, uProf, mProf, test)
# print 'finished'
# for t in testResult2:
#    f.write(str(int(t[0])) + '::' + str(int(t[1])) + ',' + str(t[2]) + '\n')
#f.close()
# f = open('result3.csv', 'w')
# testResult3 = hybridRecSys(uRat, uProf, mProf, test)
# for t in testResult3:
#     f.write(str(t[0]) + '::' + str(t[1]) + ',' + str(t[2]) + '\n')
# f.close()
# nn_cross_validation(movies, smallTraining, 5)




print 'a'
