Baseline 1:
	Get the average rating for all movies
Baseline 2:
	Get the average rating for all the movies for a single user
Baseline 3:
	Get the average rating of a single movie by all the users

Content-based recom system
- Create profile of each film
	First, I create the vocabulary of the genres.
	calculate TF and IDF based on the movies.dat and tags.dat
	for the TF i have either 0 or 1 for the genre of the film
	Once i have TF and IDF I can calculate the weights of the genres.
	weights vector for a film (w1, w2, ..., wn), n = number of genres
	w = TF x IDF

- find the content based profile of the user
	We use the trainingRatings.dat file. For each film he has rated we need to get the mean 
	of the vectors of the films that the user rated (we need to include in the average only the films that
	he/she has rated high). In this way we get a profile for a single user.

- compute cosine distance between user profile and movie profile
	compute distance for the pairs user-movie from the testFile.csv
	convert the cosine similarity to a rating e.g. 0.5 = 2.5/5

Collaborative-based recom system
- create a matrix with users and movies where i set for each user the rating he has for a movie
	......movies.......
	u| 1 2 0 0 0 0 5 0
	s| 0 0 0 2 0 1 0 4
	e| 5 4 0 0 0 0 0 1
	r|
	s|
	use sparse matrices to store the data.

-calculate cosine similarities between users
	When I calculate the cosine similarities I should only consider the users that have rated the same movies
	as the user that we are looking for.
- use weighted or simple average to calculate the rating for a movie for a user 
	use the pairs from the testFile.csv

-experiment with k for KNN
	if possible perform k-fold validation to avoid overfitting